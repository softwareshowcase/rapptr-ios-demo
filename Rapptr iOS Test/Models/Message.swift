//
//  Message.swift
//  Rapptr iOS Test
//
//  Created by Ethan Humphrey on 8/11/21.
//

import Foundation

struct Message {
    var userID: Int
    var username: String
    var avatarURL: URL?
    var text: String
    
    init(name: String, message: String, avatar_url: String, user_id: String) {
        self.userID = Int(user_id) ?? 0
        self.username = name
        self.avatarURL = URL(string: avatar_url)
        self.text = message
    }
}

struct MessageResponse: Decodable{
    var user_id: String
    var name: String
    var avatar_url: String
    var message: String
}

struct messageData: Decodable{
    let data: [MessageResponse]
}
