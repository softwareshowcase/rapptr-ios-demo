//
//  response.swift
//  Rapptr iOS Test
//
//  Created by Roberto on 6/4/22.
//

import Foundation

struct rapptrData: Decodable{
    let code: String
    let message: String
}
