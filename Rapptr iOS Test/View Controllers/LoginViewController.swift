//
//  LoginViewController.swift
//  iOSTest
//
//  Copyright © 2020 Rapptr Labs. All rights reserved.

import UIKit

class LoginViewController: UIViewController {
    
    /**
     * =========================================================================================
     * INSTRUCTIONS
     * =========================================================================================
     * 1) Make the UI look like it does in the mock-up.
     *
     * 2) Take email and password input from the user
     *
     * 3) Use the endpoint and paramters provided in LoginClient.m to perform the log in
     *
     * 4) Calculate how long the API call took in milliseconds
     *
     * 5) If the response is an error display the error in a UIAlertController
     *
     * 6) If the response is successful display the success message AND how long the API call took in milliseconds in a UIAlertController
     *
     * 7) When login is successful, tapping 'OK' in the UIAlertController should bring you back to the main menu.
     **/
    
    // MARK: - Properties
    private var client: LoginClient?

    @IBOutlet weak var inputEmail: UITextField!{
        didSet{
            let customPlaceholder =
            NSAttributedString(string: "Email",
                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "placeholderColor") ?? .gray]
            )
            inputEmail.attributedPlaceholder = customPlaceholder
        }
    }
    
    @IBOutlet weak var inputPassword: UITextField!{
        didSet{
            let customPlaceholder =
            NSAttributedString(string: "Password",
                               attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "placeholderColor") ?? .gray]
            )
            inputPassword.attributedPlaceholder = customPlaceholder
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        inputEmail.layer.cornerRadius = 8
        inputPassword.layer.cornerRadius = 8
        inputPassword.isSecureTextEntry = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func backAction(_ sender: Any) {
        client = nil
//        let mainMenuViewController = MenuViewController()
//        self.navigationController?.pushViewController(mainMenuViewController, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressLoginButton(_ sender: Any) {
        print("trying to login")
        client = LoginClient()
        let email: String = inputEmail.text ?? ""
        let password: String = inputPassword.text ?? ""
        print(email, password)
        client?.login(email: email, password: password){ code, message, time in
            DispatchQueue.main.async {
                self.showResponseAlert(code: code, message: message, time: time)
            }
        }errorHandler:{ error in
            print(error ?? "Error")
        }
    }
    
    func showResponseAlert(code: String, message: String, time: Double){
        let millis = time*1000
        let alert = UIAlertController(title: "Code: \(code)",
                                      message: message + (code == "Error" ? "" : "\nRequest Time: \(Int(millis)) ms"),
                                      preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                if code == "Success"{
                    self.backAction(self)
                }
                else{
                    print("Try again.")
                }
            }
        ))
        self.present(alert, animated: true, completion: nil)
    }
}
