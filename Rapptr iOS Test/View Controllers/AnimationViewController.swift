//
//  AnimationViewController.swift
//  iOSTest
//
//  Copyright © 2020 Rapptr Labs. All rights reserved.

import UIKit
import AVFoundation

class AnimationViewController: UIViewController {
    
    /**
     * =========================================================================================
     * INSTRUCTIONS
     * =========================================================================================
     * 1) Make the UI look like it does in the mock-up.
     *
     * 2) Logo should fade out or fade in when the user hits the Fade In or Fade Out button
     *
     * 3) User should be able to drag the logo around the screen with his/her fingers
     *
     * 4) Add a bonus to make yourself stick out. Music, color, fireworks, explosions!!! Have Swift experience? Why not write the Animation 
     *    section in Swfit to show off your skills. Anything your heart desires!
     *
     */
    
    @IBOutlet weak var rapptrLogo: UIImageView!
    @IBOutlet weak var fadeText: UIButton!
    var fade: Bool = false
    var panGesture  = UIPanGestureRecognizer()
    let synth = AVSpeechSynthesizer()
    var speakCount: Int = 0
    var firstYLogo: Float?
    var firstXLogo: Float?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(AnimationViewController.dragImg(_:)))
        rapptrLogo.isUserInteractionEnabled = true
        rapptrLogo.addGestureRecognizer(panGesture)
        firstYLogo = Float(rapptrLogo.center.y)
        firstXLogo = Float(rapptrLogo.center.x)
        title = "Animation"
    }
    
    // MARK: - Actions
    @IBAction func backAction(_ sender: Any) {
        let mainMenuViewController = MenuViewController()
        self.navigationController?.pushViewController(mainMenuViewController, animated: true)
    }
    
    @IBAction func didPressFade(_ sender: Any) {
        fade.toggle()
        if fade{
            UIImageView.animate(withDuration: 0.5,
                                delay: 0.0,
                                options: .curveEaseOut,
                                animations: {self.rapptrLogo.alpha = 0.0},
                                completion: nil
            )
            speakAction(fading: "Fading out.")
            fadeText.setTitle("FADE IN", for: .normal)
        }
        else{
            UIImageView.animate(withDuration: 0.5,
                                delay: 0.0,
                                options: .curveEaseOut,
                                animations: {self.rapptrLogo.alpha = 1.0},
                                completion: nil
            )
            speakAction(fading: "Fading in.")
            fadeText.setTitle("FADE OUT", for: .normal)
        }
    }
    
    @objc func dragImg(_ sender:UIPanGestureRecognizer){
        let translation = sender.translation(in: self.view)
        rapptrLogo.center = CGPoint(x: rapptrLogo.center.x + translation.x, y: rapptrLogo.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func speakAction(fading: String){
        var spokenString = fading
        if speakCount >= 5{
            spokenString = "Having fun?"
            speakCount = 0
        }
        var Reader: AVSpeechUtterance!
        Reader = AVSpeechUtterance(string: spokenString)
        Reader.voice = AVSpeechSynthesisVoice(identifier: "com.apple.ttsbundle.Daniel-compact")
        if let firstXLogo = firstXLogo {
            let currentXLogo = Float(rapptrLogo.center.x)
            Reader.rate = currentXLogo/firstXLogo - 0.5
        }
        if let firstYLogo = firstYLogo {
            let currentYLogo = Float(rapptrLogo.center.y)
            Reader.pitchMultiplier = firstYLogo/currentYLogo
        }
        synth.speak(Reader)
        speakCount += 1
    }

}
