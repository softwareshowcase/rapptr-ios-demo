//
//  ChatClient.swift
//  Rapptr iOS Test
//
//  Created by Ethan Humphrey on 8/11/21.
//

import Foundation

/**
 * =========================================================================================
 * INSTRUCTIONS
 * =========================================================================================
 * 1) Make a request to fetch chat data used in this app.
 *
 * 2) Using the following endpoint, make a request to fetch data
 *    URL: http://dev.rapptrlabs.com/Tests/scripts/chat_log.php
 *
 */

class ChatClient {
    
    var session: URLSession?
    
    func fetchChatData(completion: @escaping ([Message]) -> Void, error errorHandler: @escaping (String?) -> Void) {
        guard let url = URL(string: "http://dev.rapptrlabs.com/Tests/scripts/chat_log.php") else {return}
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request){ (data, response, error) in
            guard let unwrappedResponse = response as? HTTPURLResponse else{
                errorHandler("Invalid Response")
                return
            }
            switch unwrappedResponse.statusCode{
                case 200..<300:
                    print("success")
                default:
                    print("error")
            }
            if let unwrappedError = error {
                errorHandler(unwrappedError.localizedDescription)
                return
            }
            if let unwrappedData = data{
                if let messData = try? JSONDecoder().decode(messageData.self, from: unwrappedData){
                    var messages = [Message]()
                    for mess in messData.data{
                        let chatItem = Message(name: mess.name,
                                               message: mess.message,
                                               avatar_url: mess.avatar_url,
                                               user_id: mess.user_id
                        )
                        messages.append(chatItem)
                    }
                    completion(messages)
                }
            }
        }.resume()
    }
}
