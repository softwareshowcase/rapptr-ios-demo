//
//  LoginClient.swift
//  Rapptr iOS Test
//
//  Created by Ethan Humphrey on 8/11/21.
//

import Foundation

/**
 * =========================================================================================
 * INSTRUCTIONS
 * =========================================================================================
 * 1) Make a request here to login.
 *
 * 2) Using the following endpoint, make a request to login
 *    URL: http://dev.rapptrlabs.com/Tests/scripts/login.php
 *
 * 3) Don't forget, the endpoint takes two parameters 'email' and 'password'
 *
 * 4) email - info@rapptrlabs.com
 *   password - Test123
 *
*/

class LoginClient {
    
    var session: URLSession?
    
    func login(email: String, password: String, completion: @escaping (String, String, Double) -> Void, errorHandler: @escaping (String?) -> Void) {
        let startTime = CFAbsoluteTimeGetCurrent()
        guard let url = URL(string: "http://dev.rapptrlabs.com/Tests/scripts/login.php") else {return}
        
        // Build request
        var request = URLRequest(url: url)
        var components = URLComponents()
        var queryItems = [URLQueryItem]()
        queryItems.append(URLQueryItem(name: "email", value: email))
        queryItems.append(URLQueryItem(name: "password", value: password))
        components.queryItems = queryItems
        let queryItemData = components.query?.data(using: .utf8)
        request.httpBody = queryItemData
        request.httpMethod = "POST"
        // Start request
        URLSession.shared.dataTask(with: request){ (data, response, error) in
            guard let unwrappedResponse = response as? HTTPURLResponse else{
                errorHandler("Invalid Response")
                return
            }
            switch unwrappedResponse.statusCode{
                case 200..<300:
                    print("success")
                default:
                    print("error")
            }
            if let unwrappedError = error {
                errorHandler(unwrappedError.localizedDescription)
                return
            }
            if let unwrappedData = data{
                let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
                if let rapData = try? JSONDecoder().decode(rapptrData.self, from: unwrappedData){
                    completion(rapData.code, rapData.message, timeElapsed)
                }
            }
        }.resume()
    }
}
