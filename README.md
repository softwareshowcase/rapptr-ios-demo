# Rapptr iOS app development demonstration
Using a skeleton structure, I created an iOS app that:
- Makes a web request to an API and displays the response data in a chat styled user interface, including avatar images that are also loaded from the web.
- Allows a user to authenticate with a server using an email and password, with the success/failure of the authentication being passed as an alert.
- Implements a __draggable__ UIImage that can be faded in/out at the tap of a button (_turn up the volume_ to see a special effect)
# At a glance
![Alt Text](https://gitlab.com/softwareshowcase/rapptr-ios-demo/-/raw/main/chatDemo.gif)
![Alt Text](https://gitlab.com/softwareshowcase/rapptr-ios-demo/-/raw/main/loginDemo.gif)
![Alt Text](https://gitlab.com/softwareshowcase/rapptr-ios-demo/-/raw/main/animationDemo.gif)
